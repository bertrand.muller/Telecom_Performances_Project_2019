% PROJET EVALUATION DES PERFORMANCES
% Ophelien Amsler - Bertrand Muller
% Annee universitaire 2018 / 2019

% Generation d'une varibale aleatoire suivant une loi exponentielle en
% prenant en compte un processus d'arrivee
function [D] = depart(mu, A, Tfin)
    Ts = -log(rand)/mu; % Temps de service du premier client
    D = [];
    D(1) = A(1) + Ts; % Temps de depart du 1er client
    i = 1;
    while D(i) <= Tfin % Tfin est la duree d'observation donnee
        Ts = -log(rand)/mu; % Temps de service du client
        D(i+1) = max(A(i+1), D(i)) + Ts; % Temps de depart du (i+1)eme client
        i = i+1;
    end
end