% PROJET EVALUATION DES PERFORMANCES
% Ophelien Amsler - Bertrand Muller
% Annee universitaire 2018 / 2019

% Ce fichier correspond au rendu de la question 3 :
% Il permet de simuler une file d'attente en faisant varier la taille du
% buffer de 1 à 15 requêtes
% A la fin, nous avons trace le graphique representant l'evolution du taux
% de parte en fonction de la taille du buffer.

close all;
clear;

count = 1;
TauxPerteresults = [];

% Execution de la file d'attente avec un buffer limite allant de 1 a 15
% requetes
startTailleFile = 1;
endTailleFile = 15;
range = startTailleFile:endTailleFile;

for tailleFile = range
    % Donnees du sujet
    Tfin = 60*60;           % Duree d'observation : 1H
    duMoyTraitReq = 12.5;   % Duree moyenne de traitement d'une requete (en milisecondes) : 12,5

    % Calcul des variables
    lambda = 50; % Nombre de requêtes par secondes
    mu = 1000/duMoyTraitReq; % Duree moyenne de traitement en seconde

    % Simulation de la file d'attente
    [A, D, tauxPerte] = fileAttente(lambda, mu, Tfin, tailleFile);
    TauxPerteresults(count) = tauxPerte;
    
    count = count + 1;
end

% Affichage du taux de perte en fonction de la taille du buffer
plot(range, TauxPerteresults);
title('Evolution du taux de perte');