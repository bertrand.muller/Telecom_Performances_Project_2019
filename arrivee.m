% PROJET EVALUATION DES PERFORMANCES
% Ophelien Amsler - Bertrand Muller
% Annee universitaire 2018 / 2019

% Generation d'une variable aleatoire suivant une loi exponentielle à taux continu (loi de Poisson)
function [A] = arrivee(lambda, Tfin)
    A = [];
    A(1) = -log(rand)/lambda; % Temps d'arrivee du 1er client
    i = 1;
    while A(i) <= Tfin % Tfin est la duree d'observation donnee\
        u = -log(rand)/lambda; % Temps entre 2 arrivees consecutives
        A(i+1) = A(i) + u; % Temps d'arrivee du (i+1)eme client
        i = i+1;
    end
end