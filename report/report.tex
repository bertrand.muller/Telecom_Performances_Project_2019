\documentclass{tnreport}
%\documentclass[stage2a]{tnreport1} % If you are in 2nd year
%\documentclass[confidential]{tnreport1} % If you are writing confidential report

\def\reportTitle{Évaluation des performances par simulation d'une file d'attente M/M/K} % Titre du mémoire
\def\reportAuthor{Ophélien Amsler \& Bertrand Müller}

\usepackage{csquotes}
\usepackage{lipsum}
\usepackage{subcaption}
\usepackage{adjustbox}
\usepackage{dirtree}
\usepackage{tikz}
\usepackage{multirow}
\usetikzlibrary{trees}
\usepackage{tikz-qtree}
\usepackage{makecell}
\usepackage{hhline}
\usepackage{colortbl}
\usepackage[most]{tcolorbox}
\usepackage{pdfpages}
\usepackage{draftwatermark}
\usepackage[stable]{footmisc}
\usepackage{hhline}
\usepackage{float}
\usepackage{graphicx}

\begin{document}

\maketitle

\clearpage

\renewcommand{\baselinestretch}{0.5}\normalsize
\tableofcontents
\renewcommand{\baselinestretch}{1.0}\normalsize

\clearpage


\chapter{Présentation de la méthode d'évaluation des performances par simulation}
Durant notre formation, nous avons eu l'occasion d'évaluer les performances de différents systèmes via des formules mathématiques théoriques.\\
L'objectif de ce projet est de réaliser la simulation d'un système pour obtenir des résultats expérimentaux. Nous pourrons ensuite les comparer avec les résultats théoriques.\\
\\
Pour obtenir ces résultats expérimentaux, nous devons utiliser des variables aléatoires pour simuler chacun des éléments du système. En répétant cette expérience un grand nombre de fois (i.e. sur une longue durée), nous pourrons ainsi faire des calculs précis en exploitant les données obtenues.\\
\\
Dans ce rapport, nous allons simuler une file M/M/K à l'aide du logiciel Matlab. Cette simulation nous permettra de calculer des résultats expérimentaux en vue de les comparer avec les résultats théoriques. 

\chapter{Simulation d'une variable aléatoire et applications}
Pour simuler une variable aléatoire suivant une fonction exponentielle, nous allons nous appuyer sur la propriété suivante :

\setlength{\fboxsep}{1em}
\fbox{%
	\parbox{\textwidth}{%
			Si X est une variable aléatoire de loi U(]a, b[), alors les variables aléatoires $-\frac{1}{\lambda} log(\frac{X-a}{b-a})$ et $-\frac{1}{\lambda} log(\frac{b-X}{b-a})$ suivant la même loi Exp($\lambda$).\\
	}%
}

Pour générer une variable uniforme U(]0,1[), nous allons utiliser la fonction \textit{rand()} de Matlab. Celle-ci retourne un nombre uniformément distribué entre 0 et 1. Grâce à la propriété ci-dessus, nous pouvons simuler une variable aléatoire qui suit une loi exponentielle de paramètre $\lambda$ avec la formule suivante : $Exp(\lambda) = -\frac{1}{\lambda} log(X)$, où X remplace la fonction \textit{rand()} de Matlab.\\
\\
\textbf{Applications}\\ \\
Une des applications de la simulation des variables aléatoires consiste à mesurer le temps séparant deux clients arrivant à l'entrée du système.\\
\\
Pour réaliser cette simulation, nous avons créé une fonction \textit{arrivee} qui admet comme paramètres : $\lambda$ de la loi exponentielle et \textit{Tfin} la durée d'observation du système.\\ 
Nous calculons la variable aléatoire \textit{A} qui contient l'instant d'arrivée de chaque client.\\

\lstinputlisting[firstline=5, language=Matlab, caption=Code Matlab pour simuler un processus d'arrivée suivant une loi de Poisson]{../arrivee.m}

\newpage
Sur le même modèle, nous pouvons mesurer le temps de service d'un serveur, qui suit une loi exponentielle de paramètre $\mu$. Cette fois-ci, la fonction a besoin des instants d'arrivée pour chacun des clients. Ces données sont fournies comme paramètre de la fonction.
\\
\lstinputlisting[firstline=5, language=Matlab, caption=Code Matlab pour simuler un processus de départ suivant une loi expontentielle]{../depart.m}

Enfin, si nous souhaitons utiliser une file d'attente avec un buffer limité, nous devons fusionner les deux fonctions précédentes. Le résultat de cette fusion est le suivant :\\

\lstinputlisting[firstline=5, language=Matlab, caption=Code Matlab pour simuler une file d'attente]{../fileAttente.m}

\chapter{Simulation d'une file d'attente M/M/K}

Nous allons procéder à la simulation d'une file d'attente M/M/K. Une file d'attente de ce type est composée d'un processus d'arrivée et d'un processus de départ.\\
\\
Nous allons modéliser le processus d'arrivée par une loi de Poisson, c'est-à-dire une loi exponentionnelle avec un taux d'arrivée constant nommé $\lambda$. En ce qui concerne le processus de départ, nous allons utiliser une loi exponentielle de paramètre $\mu$.\\
\\
Pour simuler une file M/M/K, il faut multiplier le paramètre $\mu$ par K

Le code Matlab ci-dessous permet de générer une file M/M/1. La fonction \textit{fileAttente} est décrite dans la partie 2.
\\
\lstinputlisting[language=Matlab, caption=Code Matlab pour simuler une file M\/M\/1]{simulation_MMK.m}

\newpage
Le graphique ci-dessous donne le nombre de clients obtenus par les processus d'arrivée et de départ en fonction du temps (exprimé en secondes).\\
Le code utilisé pour réaliser le graphique est présenté dans le code Matlab ci-dessus.
\begin{figure}[h]
	\centering
	\fboxsep=1.2pt
	\includegraphics[scale=0.8]{nb_clients_arrivee_depart}
	\caption{Affichage des 100 premiers clients dans la file d'attente (M/M/1)}
	\label{fig:graphique_entree_sortie}
\end{figure}

\chapter{Calcul de différentes mesures de performance}
Grâce à l'application réalisée précédemment, nous allons pouvoir calculer les performances du système.

Nous allons calculer les indicateurs suivants :
\begin{itemize}
	\item Le temps moyen d'attente
	\item Le temps moyen de service
	\item Le nombre moyen de requêtes dans le système
	\item Le débit en sortie
	\item Le temps moyen de séjour
	\item Le taux d'occupation du serveur
	\\
\end{itemize}
 
Dans la suite de ce rapport, \textit{sizeD} représente le nombre de clients sortis du système et \textit{Tfin} le temps total d'observation.

\section{Temps moyen d'attente}
Le temps moyen d'attente correspond au temps pour lequel un client va devoir attendre avant d'être traité par le serveur.\\
\\
\textbf{Calcul théorique (M/M/1)}\\
Voici la formule du calcul théorique : $W = \frac{\lambda}{(\mu * (\mu - \lambda)))}$
\\
\\
\textbf{Mesure applicative}\\
Le calcul du temps moyen d'attente s'effectue en faisant la différence entre l'instant de départ du client précédent et l'arrivée du client en cours. On ne prend uniquement la valeur en compte que si le temps est positif (si le temps est négatif, cela veut dire que le système est en attente de l'arrivée d'un nouveau client).\\

\begin{lstlisting}[language=Matlab, frame=single]
sommeW = 0;
for i = 2:sizeD
	tempsW = D(i-1) - A(i);
	if tempsW > 0
		sommeW = sommeW + tempsW;
	end
end
W = sommeW/(sizeD-1);
\end{lstlisting}

\section{Temps moyen de service}
Le temps moyen de service correspond au temps de traitement d'un client par le serveur.\\
\\
\textbf{Calcul théorique (M/M/1)}\\
Voici la formule du calcul théorique : $S = \frac{1}{\mu}$
\\
\\
\textbf{Mesure applicative}\\
Le calcul du temps moyen de service s'effectue en faisant la différence entre l'instant de départ du client et du maximum entre l'arrivée de ce client ou du départ du client précédent.\\
\begin{lstlisting}[language=Matlab, frame=single]
sommeS = 0;
for i = 2:sizeD
	sommeS = sommeS + D(i) - max(A(i), D(i-1));
end
S = sommeS/sizeD;
\end{lstlisting}

\section{Nombre moyen de requêtes dans le système}
Le nombre moyen de requêtes dans le système correspond à la moyenne du nombre de clients présents dans le système pour toute la durée d'observation.\\
\\
\textbf{Calcul théorique (M/M/1)}\\
Voici la formule du calcul théorique : $Q = \frac{\lambda}{\mu - \lambda}$
\\
\textbf{Mesure applicative}\\
Le calcul du nombre moyen de requêtes dans le système s'effectue en faisant la division du temps moyen d'attente et du temps moyen de service.\\
\begin{lstlisting}[language=Matlab, frame=single]
Q = W/S
\end{lstlisting}

\section{Débit en sortie}
Le débit en sortie correspond au nombre moyen de clients traités en une seconde par le serveur.\\
\\
\textbf{Calcul théorique (M/M/1)}\\
Voici la formule du calcul théorique : $X = \lambda$
\\
\\
\textbf{Mesure applicative}\\
Le calcul du débit en sortie s'effectue en faisant le rapport entre le nombre de requêtes sorties et le temps total d'observation.\\
\begin{lstlisting}[language=Matlab, frame=single]
X = sizeD/Tfin
\end{lstlisting}

\section{Temps moyen de séjour}
Le temps moyen de séjour correspond à la durée moyenne qu'un client va passer dans le système.\\
\\
\textbf{Calcul théorique (M/M/1)}\\
Voici la formule du calcul théorique : $R = \frac{1}{\mu - \lambda}$
\\
\\
\textbf{Mesure applicative}\\
Le calcul du temps moyen de séjour s'effectue en faisant la différence entre l'instant de départ du client et son temps d'arrivée.\\

\begin{lstlisting}[language=Matlab, frame=single]
sommeR = 0;
for i = 1:sizeD
	sommeR = sommeR + D(i) - A(i);
end
R = sommeR/sizeD;
\end{lstlisting}

\section{Taux d'occupation du serveur}
Le taux d'occupation du serveur correspond au rapport entre la durée pendant laquelle le serveur est utilisée et le temps total d'observation.\\
\\
\textbf{Calcul théorique (M/M/1)}\\
Voici la formule du calcul théorique : $\rho = \frac{\lambda}{\mu}$
\\
\\
\textbf{Mesure applicative}\\
Le calcul du taux d'occupation du serveur s'effectue en faisant la différence entre l'instant d'arrivée du client et l'instant de départ du client précédent. Nous tenons uniquement compte des valeurs positives. En effet, si la valeur est négative,  cela signifie que le système est resté occupé.\\

\begin{lstlisting}[language=Matlab, frame=single]
tempsNonUtilise = 0;
for i = 2:sizeD
	tempsRo = A(i) - D(i-1);
	if tempsRo > 0
		tempsNonUtilise = tempsNonUtilise + tempsRo;
	end
end
Ro = (Tfin - tempsNonUtilise)/Tfin
\end{lstlisting}

\chapter{Conclusions}
Dans ce rapport, nous avons vu comment simuler une variable aléatoire suivant une loi exponentielle et l'utiliser pour simuler des processus de départ et d'arrivée. Ensuite, nous avons réalisé la simulation d'une file d'attente M/M/K. Enfin, nous avons calculé des indicateurs de performance sur notre file d'attente.\\
\\
Les différentes mesures expérimentales réalisées sont très proches des valeurs théoriques. Nous pouvons donc en déduire qu'il est possible de simuler une file d'attente à l'aide de Matlab.\\
\\
Voici les résultats obtenus en faisant varier le nombre de requêtes entre 40 et 80 par seconde.\\
\begin{figure}[h]
	\hspace*{-4.2cm}
	\includegraphics[scale=0.45]{graphique_40_80}
	\caption{Évolution des différents indicateurs en fonction du nombre de requêtes}
	\label{fig:graphique_40_80}
\end{figure}

On constate que l'évolution du taux d'occupation du serveur et que le débit en sortie sont proportionnels au nombre de requêtes. Cela est cohérent puisque que le serveur reste inchangé, il met donc plus de temps à traiter les clients lorsqu'il y en a plus.\\
Le temps moyen de service reste stable, cela est normal puisque le serveur met toujours autant de temps à traiter une requête.\\
Enfin, le nombre moyen de requêtes dans le système, le temps moyen d'attente et le temps moyen de séjour restent stables pour un traitement jusqu'à 70 requêtes par seconde avant d'augmenter de manière exponentielle aux alentours des 80 requêtes par seconde. Cela s'explique par le fait que le serveur arrive à saturation. En effet, il peut traiter 80 requêtes par seconde. S'il reçoit 80 clients par seconde, il arrive quasiment à saturation, d'où la croissance exponentielle.\\
\\
\\
Désormais, nous allons nous intéresser au taux de perte lorsque la taille du buffer n'est plus illimitée. Nous allons faire varier la taille du buffer entre 1 et 15.
\begin{figure}[h]
	\includegraphics[scale=0.8]{graphique_tauxPerte}
	\caption{Évolution du taux de perte en fonction de la taille du buffer}
	\label{fig:graphique_tauxPerte}
\end{figure}

\end{document}
