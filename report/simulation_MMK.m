% Donnees du sujet
Tfin = 3600 ;           % Duree d'observation : 1H
duMoyTraitReq = 12.5;   % Duree moyenne de traitement d'une requete (en milisecondes) : 12,5
reqParSec = 50;         % Nombre de requetes par secondes
K = 1;                  % Nombre de serveurs dans la file
tailleBuffer = Inf;     % Taille du buffer

% Calcul des variables
lambda = reqParSec;
mu = 1000/duMoyTraitReq*K; % Duree moyenne de traitement en seconde

% Simulation d'une file d'attente M/M/1
[A, D] = fileAttente(lambda, mu, Tfin, tailleBuffer);

% Affichage du graphique
hold on;
x = 1:100;
stairs(A(x), x);
stairs(D(x), x);
legend("A - Processus d'arrivee", "D - processus de depart", "location", "northwest");
xlabel("Temps");
ylabel("Nombre de clients");