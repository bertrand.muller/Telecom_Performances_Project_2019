% PROJET EVALUATION DES PERFORMANCES
% Ophelien Amsler - Bertrand Muller
% Annee universitaire 2018 / 2019

% Ce fichier correspond au rendu de la question 2 (qui reprend la question 1) :
% Il permet de simuler une file d'attente en faisant varier le nombre de
% requetes de 40 a 80.
% A la fin, nous avons trace les differents graphiques pour chaque
% indicateurs de performance.

close all;
clear;

count = 1;
Wresults = [];
Rresults = [];
Sresults = [];
ROresults = [];
Qresults = [];
Xresults = [];

% Execution de la file d'attente avec un nombre de requetes allant de  40 a 80 
% avec un pas de 2
stepReq = 2;
startReq = 40;
endReq = 80;
range = startReq:stepReq:endReq;

for reqParSec = range
    % Donnees du sujet
    Tfin = 60*60;           % Duree d'observation : 1H
    duMoyTraitReq = 12.5;   % Duree moyenne de traitement d'une requete (en milisecondes) : 12,5

    % Calcul des variables
    lambda = reqParSec;
    mu = 1000/duMoyTraitReq; % Duree moyenne de traitement en seconde

    % Simulation de la file d'attente
    A = arrivee(lambda, Tfin);
    D = depart(mu, A, Tfin);
    
    sizeA = size(A, 2); % Nombre de clients arrives
    sizeD = size(D, 2); % Nombre de clients traites par le serveur

    figure;
    hold on;
    x = 1:100;
    stairs(A(x), x);
    stairs(D(x), x);
    legend("A - Processus d'arrivée", "D - processus de depart", "location", "northwest");
    xlabel("Temps");
    ylabel("Nombre de clients");

    %%%%%%%%%%%%%%%%%%%%%%%
    % Temps moyen attente %
    %%%%%%%%%%%%%%%%%%%%%%%
    Wt = lambda/(mu*(mu-lambda)); % theorique
    sommeW = 0;
    for i = 2:sizeD
        tempsW = D(i-1) - A(i);
        if tempsW > 0
            sommeW = sommeW + tempsW;
        end
    end
    W = sommeW/(sizeD-1); % obtenu
    Wresults(count) = W;    

    %%%%%%%%%%%%%%%%%%%%%%%%%%
    % Temps moyen de service %
    %%%%%%%%%%%%%%%%%%%%%%%%%%
    St = 1/mu; % theorique
    sommeS = 0;
    for i = 2:sizeD
        sommeS = sommeS + D(i) - max(A(i), D(i-1));
    end
    S = sommeS/sizeD;
    Sresults(count) = S;

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Nombre moyen de requetes dans le systeme %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    Qt = lambda/(mu-lambda); % theorique
    Q = W/S; % obtenu
    Qresults(count) = Q;

    %%%%%%%%%%%%%%%%%%%
    % Debit en sortie %
    %%%%%%%%%%%%%%%%%%%
    Xt = lambda; % theorique
    X = sizeD/Tfin; % obtenu
    Xresults(count) = X;

    %%%%%%%%%%%%%%%%%%%%%%%%%
    % Temps moyen de sejour %
    %%%%%%%%%%%%%%%%%%%%%%%%%
    Rt = 1/(mu-lambda); % theorique
    sommeR = 0;
    for i = 1:sizeD
        sommeR = sommeR + D(i) - A(i);
    end
    R = sommeR/sizeD; % obtenu
    Rresults(count) = R;

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Taux occupation du serveur %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    Rot = lambda/mu; % theorique
    tempsNonUtilise = 0;
    for i = 2:sizeD
        tempsRo = A(i) - D(i-1);
        if tempsRo > 0
            tempsNonUtilise = tempsNonUtilise + tempsRo;
        end
    end
    Ro = (Tfin - tempsNonUtilise)/Tfin; % obtenu 
    ROresults(count) = Ro;
    
    count = count + 1;
end

% Graphiques
figure
subplot(3,2,1);
plot(range, Qresults);
title('Evolution de Q');

subplot(3,2,2);
plot(range, ROresults);
title('Evolution de Ro');

subplot(3,2,3);
plot(range, Rresults);
title('Evolution de R');

subplot(3,2,4);
plot(range, Sresults);
title('Evolution de S');

subplot(3,2,5);
plot(range, Wresults);
title('Evolution de W');

subplot(3,2,6);
plot(range, Xresults);
title('Evolution de X');