% PROJET EVALUATION DES PERFORMANCES
% Ophelien Amsler - Bertrand Muller
% Annee universitaire 2018 / 2019

% Generation d'une file d'attente
function [A, D, tauxPerte] = fileAttente(lambda, mu, Tfin, tailleFile)
    A = [];
    D = [];
    
    A(1) = -log(rand)/lambda; % Temps d'arrivee du 1er client
    
    Ts = -log(rand)/mu; % Temps de service du premier client
    D(1) = A(1) + Ts; % Temps de depart du 1er client
    
    iA = 1; iD = 1; % Indices des variables aleatoires
    perte = 0; nbRequetes = 0; % Initialisation des compteurs
    while A(iA) <= Tfin && D(iD) <= Tfin % Tfin est la duree d'observation donnee
        nbRequetes = nbRequetes+1;
        u = -log(rand)/lambda; % Temps entre 2 arrivees consecutives
        A(iA+1) = A(iA) + u; % Temps d'arrivee du (i+1)eme client
        iA = iA+1;
        
        if iA - iD > tailleFile % Si la file a ateint sa capacite maximale
            perte = perte + 1;
        end
                
        while D(iD) <= A(iA) && iD <= iA % Tant qu'il y a des requetes a traiter
            Ts = -log(rand)/mu; % Temps de service du client
            D(iD+1) = max(A(iD+1), D(iD)) + Ts; % Temps de depart du (i+1)eme client
            iD = iD+1;
        end
    end
    tauxPerte = perte/nbRequetes; % Calcul du taux de perte
end